const express = require("express");
const { body, validationResult } = require("express-validator");
const admin = require("firebase-admin");

const serviceAccount = require("./learnia-dev3-firebase-adminsdk-itimw-c0e352d37b.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();

const usersCollection = db.collection("users");

const app = express();
app.use(express.json());
const port = 3000;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

// Create a User
// we need to validate also

// Middleware to validate request body for creating a user
const validateUser = [
  body("username").notEmpty().withMessage("username is required"),
  body("email").isEmail().withMessage("Invalid email"),
  body("password")
    .isLength({ min: 6 })
    .withMessage("Password must be at least 6 characters long"),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

app.post("/user", validateUser, (req, res) => {
  const { username, email, password } = req.body;

  // const username = req.body.username
  // const email = req.body.email

  console.log(req.body);

  // TODO save data in DB

  const newUser = {
    username,
    password,
    email
  };

  usersCollection
    .add(newUser)
    .then((docRef) => {
      console.log("User added with ID:", docRef.id);
    })
    .catch((error) => {
      console.error("Error adding user:", error);
    });

  res.send({ status: "successfull" });
});

// Update user
app.put("/user", validateUser, (req, res) => {
  const { username, email, password } = req.body;

  // const username = req.body.username
  // const email = req.body.email

  console.log(req.body);

  // TODO save data in DB

  res.send({ status: "successfull" });
});

app.delete("/user/:id", (req, res) => {
  console.log(req.params.id);
  // TODO delete user from database
  res.send({ status: "successfull" });
});

app.get("/user/:id", (req, res) => {
  console.log(req.params.id);
  // TODO delete user from database
  res.send({ status: "successfull" });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
